This is the initial release of the Toolbelt CLI. A tool that provides a entry point to all your scripts, CLI commands and tools so you never have to remember what to run and where from again.  

This release should be fully functional on most desktop platforms, but it is a very early build and there might be a number of bugs and missing features.

If you have any issues please report them here: https://gitlab.com/tool.belt/toolbelt/-/issues