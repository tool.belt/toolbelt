#!/usr/bin/env bash

echo "Building for Linux"
env GOOS=linux GOARCH=amd64 go build -o build/toolbelt-linux-amd64

echo "Building for Windows"
env GOOS=windows GOARCH=amd64 go build -o build/toolbelt-windows-amd64.exe

echo "Building for Mac OSX"
env GOOS=darwin GOARCH=amd64 go build -o build/toolbelt-darwin-amd64