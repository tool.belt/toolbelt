// +heroku goVersion go1.14
module gitlab.com/tool.belt/toolbelt

go 1.14

require gopkg.in/yaml.v2 v2.2.8