package definitions

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/tool.belt/toolbelt/errors"
	"gitlab.com/tool.belt/toolbelt/log"
	"gopkg.in/yaml.v2"
)

type Definitions []Definition

const (
	ErrDefFileMissing = errors.ConstError("unable to find a tb.sdef in current directory or parents")
)

const definitionsFileName = "tb.sdef"

type Definition struct {
	Name        string
	Description string
	Help        string
	Argument    string
	Type        string
	Command     string
	Script      string
	Idx         int
}

func Load() (Definitions, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	defFile := findDefinitionsFile(pwd)
	if defFile == "" {
		return nil, ErrDefFileMissing
	}

	defs := []Definition{}

	data, err := ioutil.ReadFile(defFile)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(data, &defs); err != nil {
		return nil, err
	}

	for i, def := range defs {
		def.Idx = i
	}

	return defs, nil
}

func findDefinitionsFile(searchPath string) string {
	log.Verbosef("current search path: %s", searchPath)

	defFile := filepath.Join(searchPath, definitionsFileName)

	_, err := os.Stat(defFile)
	if os.IsNotExist(err) {
		parentDir := filepath.Dir(searchPath)
		if parentDir == searchPath || searchPath == "." || searchPath == "/" {
			return ""
		}

		return findDefinitionsFile(filepath.Dir(searchPath))
	}

	return defFile
}

func (d *Definitions) GetDefByArg(arg string) *Definition {
	for _, def := range *d {
		if def.Argument == arg {
			return &def
		}
	}

	return nil
}
