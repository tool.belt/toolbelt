// +build !windows

package tools

import (
	"fmt"
	"os/exec"
)

func execute(command *Tool, commandArgs []string) *exec.Cmd {
	cmdLine := makeCmdLine(append([]string{command.Executable}, commandArgs...), false)

	fmt.Printf("\nExecuting `%s`\n\n", cmdLine)

	return exec.Command(command.Executable, commandArgs...)
}
