package tools

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"

	"gitlab.com/tool.belt/toolbelt/definitions"
	"gitlab.com/tool.belt/toolbelt/errors"
)

const (
	ErrToolNotFound = errors.ConstError("tool not found")
)

type Tool struct {
	Name             string
	Executable       string
	CommandArguments []string
	ScriptArguments  []string
	UseRawCmdLine    bool
}

var supportedTools = []Tool{
	{
		Name:             "zsh",
		Executable:       "zsh",
		CommandArguments: []string{"-c"},
	},
	{
		Name:             "sh",
		Executable:       "sh",
		CommandArguments: []string{"-l", "-c"}, //-l is a workaround for sh on windows where PS1 envvar contains __git_ps1 and this isn't source in non-login shell
	},
	{
		Name:             "python",
		Executable:       "python",
		CommandArguments: []string{"-c"},
	},
	{
		Name:             "cmd",
		Executable:       "cmd.exe",
		CommandArguments: []string{"/Q", "/C"},
		ScriptArguments:  []string{"/Q", "/C"},
		UseRawCmdLine:    true,
	},
	{
		Name:             "powershell",
		Executable:       "powershell",
		CommandArguments: []string{"-Command"},
		ScriptArguments:  []string{"-File"},
		UseRawCmdLine:    true,
	},
}

func ExecTool(def *definitions.Definition, extraArgs []string) (int, error) {
	var command *Tool

	for _, c := range supportedTools {
		if c.Name == def.Type {
			command = &c
			break
		}
	}

	if command == nil {
		return 1, ErrToolNotFound.Wrap(fmt.Errorf("unable to find %s", def.Type))
	}

	var execCommand string
	var execArgs []string

	if def.Command != "" {
		execCommand = def.Command
		execArgs = command.CommandArguments
	} else {
		execCommand = def.Script
		execArgs = command.ScriptArguments
	}

	commandArgs := append(execArgs, append([]string{execCommand}, extraArgs...)...)

	cmd := execute(command, commandArgs)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok {
			if status, ok := exitErr.Sys().(syscall.WaitStatus); ok {
				return int(status.ExitStatus()), nil
			}
		} else {
			return 1, err
		}
	}

	return 0, nil
}

func makeCmdLine(args []string, useRawCmdLine bool) string {
	var s string
	for _, v := range args {
		if s != "" {
			s += " "
		}

		if useRawCmdLine {
			s += v
		} else {
			s += escapeArg(v)
		}
	}
	return s
}

func escapeArg(s string) string {
	if len(s) == 0 {
		return "\"\""
	}
	n := len(s)
	hasSpace := false
	for i := 0; i < len(s); i++ {
		switch s[i] {
		case '"', '\\':
			n++
		case ' ', '\t':
			hasSpace = true
		}
	}
	if hasSpace {
		n += 2
	}
	if n == len(s) {
		return s
	}

	qs := make([]byte, n)
	j := 0
	if hasSpace {
		qs[j] = '"'
		j++
	}
	slashes := 0
	for i := 0; i < len(s); i++ {
		switch s[i] {
		default:
			slashes = 0
			qs[j] = s[i]
		case '\\':
			slashes++
			qs[j] = s[i]
		case '"':
			for ; slashes > 0; slashes-- {
				qs[j] = '\\'
				j++
			}
			qs[j] = '\\'
			j++
			qs[j] = s[i]
		}
		j++
	}
	if hasSpace {
		for ; slashes > 0; slashes-- {
			qs[j] = '\\'
			j++
		}
		qs[j] = '"'
		j++
	}
	return string(qs[:j])
}
