package tools

import (
	"fmt"
	"os/exec"
	"syscall"
)

func execute(command *Tool, commandArgs []string) *exec.Cmd {
	cmdLine := makeCmdLine(append([]string{command.Executable}, commandArgs...), command.UseRawCmdLine)

	fmt.Printf("\nExecuting `%s`\n\n", cmdLine)

	var args []string
	if !command.UseRawCmdLine {
		args = commandArgs
	}

	cmd := exec.Command(command.Executable, args...)

	if command.UseRawCmdLine {
		cmd.SysProcAttr = &syscall.SysProcAttr{
			CmdLine: cmdLine,
		}
	}

	return cmd
}
