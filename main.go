package main

import (
	"os"

	"errors"

	"gitlab.com/tool.belt/toolbelt/definitions"
	"gitlab.com/tool.belt/toolbelt/environment"
	"gitlab.com/tool.belt/toolbelt/log"
	"gitlab.com/tool.belt/toolbelt/renderers"
	"gitlab.com/tool.belt/toolbelt/tools"
)

func main() {
	exitCode := 0

	opts := environment.GetOptions(os.Args[1:])
	log.Init(opts)

	defs, err := definitions.Load()
	if err != nil {
		log.Printf("%v", err)
		exitCode = 1
		defs = []definitions.Definition{}
	}

	r := renderers.GetRenderer(defs, opts)
	if err := r.Render(); err != nil {
		switch {
		case errors.Is(err, renderers.ErrExit):
			os.Exit(exitCode)
		default:
			log.Fatalf("%v", err)
		}
	}

	exitCode, err = tools.ExecTool(r.GetSelectedDefinition(), opts.ExtraArguments)
	if err != nil {
		log.Fatalf("%v", err)
	}

	os.Exit(exitCode)
}
