package environment

import "strings"

type Options struct {
	Verbose        bool
	Help           bool
	ToolArg        string
	ExtraArguments []string
}

func GetOptions(args []string) Options {
	opts := Options{}

	for i, arg := range args {
		if strings.HasPrefix(arg, "--") {
			arg = strings.TrimPrefix(arg, "--")
			switch arg {
			case "":
				opts.ExtraArguments = args[i+1:]
				return opts
			case "verbose":
				opts.Verbose = true
				break
			case "help":
				opts.Help = true
				break
			}

		} else if strings.HasPrefix(arg, "-") {
			opts.ToolArg = strings.TrimPrefix(arg, "-")
		}
	}

	return opts
}
