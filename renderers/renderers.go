package renderers

import (
	"gitlab.com/tool.belt/toolbelt/definitions"
	"gitlab.com/tool.belt/toolbelt/environment"
	"gitlab.com/tool.belt/toolbelt/errors"
)

const (
	ErrExit         = errors.ConstError("exit")
	ErrToolNotFound = errors.ConstError("unable to find tool")
)

type Renderer interface {
	Render() error
	GetSelectedDefinition() *definitions.Definition
}

func GetRenderer(defs []definitions.Definition, opts environment.Options) Renderer {
	if len(defs) == 0 || opts.Help {
		return newHelp(defs, opts)
	}

	if opts.ToolArg == "" {
		return newInteractive(defs, opts)
	}

	return newCli(defs, opts)
}
