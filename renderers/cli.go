package renderers

import (
	"gitlab.com/tool.belt/toolbelt/definitions"
	"gitlab.com/tool.belt/toolbelt/environment"
)

type cli struct {
	opts        environment.Options
	defs        definitions.Definitions
	selectedDef *definitions.Definition
}

func newCli(defs definitions.Definitions, opts environment.Options) Renderer {
	return &cli{
		opts: opts,
		defs: defs,
	}
}

func (c *cli) Render() error {
	def := c.defs.GetDefByArg(c.opts.ToolArg)
	if def == nil {
		return ErrToolNotFound
	}

	c.selectedDef = def
	return nil
}

func (c *cli) GetSelectedDefinition() *definitions.Definition {
	return c.selectedDef
}
