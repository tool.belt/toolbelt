package renderers

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/tool.belt/toolbelt/definitions"
	"gitlab.com/tool.belt/toolbelt/environment"
)

type interactive struct {
	opts      environment.Options
	defs      []definitions.Definition
	toolIndex int
}

func newInteractive(defs []definitions.Definition, opts environment.Options) Renderer {
	i := &interactive{
		opts:      opts,
		defs:      defs,
		toolIndex: -1,
	}

	return i
}

func (i *interactive) Render() error {
	return i.handleInteractiveSelection()
}

func (i *interactive) GetSelectedDefinition() *definitions.Definition {
	if i.toolIndex >= 0 {
		return &i.defs[i.toolIndex]
	}

	return nil
}

func (i *interactive) handleInteractiveSelection() error {
	fmt.Printf("Choose one of the tools below to run\n\n")

	option := displayOptions(i.defs)
	if option < 0 {
		return ErrExit
	}

	i.toolIndex = option
	return nil
}

func displayOptions(defs []definitions.Definition) int {
	for i, def := range defs {
		fmt.Printf("%d) %s - %s \n", i+1, def.Name, def.Description)
	}

	fmt.Printf("%d) Exit\n\n", len(defs)+1)

	fmt.Println("Please enter the number of the tool to use:")

	r := bufio.NewReader(os.Stdin)
	input, _ := r.ReadString('\n')
	input = strings.TrimSpace(input)

	option, err := strconv.Atoi(input)
	if err != nil {
		fmt.Printf("Input is invalid!\n\n")
		return displayOptions(defs)
	}

	option--
	if option == len(defs) {
		return -1
	} else if option > len(defs) {
		fmt.Printf("Selection not valid!\n\n")
		return displayOptions(defs)
	}

	return option
}
