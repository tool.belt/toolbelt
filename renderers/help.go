package renderers

import (
	"fmt"

	"gitlab.com/tool.belt/toolbelt/definitions"
	"gitlab.com/tool.belt/toolbelt/environment"
)

// TODO: maybe define this somewhere that can populate the README.md and this to avoid needing to keep in sync
const toolbeltDescription = `
Toolbelt is your collection of tools and scripts all in one handy location
For a full rundown on its capabilities visit https://gitlab.com/tool.belt/toolbelt
`

const basicHelp = `
Toolbelt requires a definitions file called tb.sdef in the current working directory or any of its parent directories to function.
The definitions file defines a set of tools accessible via the toolbelt.
An example tb.sdef file (in YAML format) is below:

---
- name: Example1
  description: The first example tool.
  help: this tool will run the command defined below
  argument: e1
  type: sh
  command: echo "Hello from the example1 tool"
- name: Example2
  description: The second example tool.
  help: this tool will run the script in the path defined below relative to the tb.sdef file
  argument: e2
  type: python
  script: scripts/example2.py

This file can contain as many different tools as needed with the only requirement being to avoid duplicate argument values, as the first tool matching an argument will be used.
`

const toolbeltHelp = `
Usage: toolbelt [OPTIONS] [ARG] -- [TOOL ARGS]

Options:
	--help		For help on toolbelt or a particular tool.
	--verbose	Enable verbose logging for toolbelt.

Arg: Is the argument of the corresponding tool in the toolbelt tb.sdef file. use --help with an arg to get help on that specific argument.
If no argument is specified the cli tool will run in interactive mode showing all available options and allowing one to be chosen.

Tool Args: any arguments after the optional -- delimiter will be passed directly through to the underlying tool.
`

type help struct {
	opts environment.Options
	defs definitions.Definitions
}

func newHelp(defs definitions.Definitions, opts environment.Options) Renderer {
	return &help{
		opts: opts,
		defs: defs,
	}
}

func (h *help) Render() error {
	if len(h.defs) == 0 {
		printBasicHelp()
	} else if h.opts.ToolArg == "" {
		printHelp(h.defs)
	} else {
		if err := h.printToolHelp(); err != nil {
			return err
		}
	}

	return ErrExit
}

func (h *help) GetSelectedDefinition() *definitions.Definition {
	return nil
}

func printBasicHelp() {
	fmt.Printf(toolbeltDescription)
	fmt.Printf(basicHelp)
}

func printHelp(defs definitions.Definitions) {
	fmt.Printf(toolbeltDescription)
	fmt.Printf(toolbeltHelp)

	fmt.Printf("\nAvailable tools:\n\n")
	for _, def := range defs {
		fmt.Printf("-%s) %s - %s\n", def.Argument, def.Name, def.Description)
	}
}

func (h *help) printToolHelp() error {
	def := h.defs.GetDefByArg(h.opts.ToolArg)
	if def == nil {
		return ErrToolNotFound
	}

	fmt.Printf("Usage: toolbelt [OPTIONS] -%s -- [TOOL ARGS]\n", def.Argument)

	var command string
	if def.Command != "" {
		command = def.Command
	} else {
		command = def.Script
	}

	fmt.Printf(`
%s can be run with argument -%s or selecting option %d in interactive mode.

%s - %s
Command executed - %s [TOOL ARGS]
 
%s 
`, def.Name, def.Argument, def.Idx, def.Name, def.Description, command, def.Help)

	return nil
}
