# Toolbelt

Toolbelt provides a entry point to all your scripts, CLI commands and tools so you never have to remember what to run and where from again.

* Toolbelt helps you remember and organise all those scripts and commands you need to run on a day to day basis.
* Provides a centralised location for discovering and running common commands and tools for your workstation, project or working directory.
* Features an interactive mode for ease of use and discovery.
* Allows the pass through of arguments to underyling scripts/commands/tools.
* Supports a number of different execution environments such as sh, zsh, python & more.

#TODO: ## Insert screenshot of the different modes

## Installation

Basic installation:

 `go get -u gitlab.com/tool.belt/toolbelt`

Alternative:

Download a prebuilt binary from the releases section:

https://gitlab.com/tool.belt/toolbelt/-/releases

## Setup

Toolbelt requires some initial setup before it is useful.  

Toolbelt requires a definitions file called tb.sdef in the current working directory or any of its parent directories to function.
The definitions file defines a set of tools accessible via the toolbelt.
An example tb.sdef file (in YAML format) is below:

```yaml
---
- name: Example1
  description: The first example tool.
  help: this tool will run the command defined below
  argument: e1
  type: sh
  command: echo "Hello from the example1 tool"
- name: Example2
  description: The second example tool.
  help: this tool will run the script in the path defined below relative to the tb.sdef file
  argument: e2
  type: python
  script: scripts/example2.py
```

This file can contain as many different tools as needed with the only requirement being to avoid duplicate argument values, as the first tool matching an argument will be used.

The `type` field specifies the execution environment to run the script or command in.  

The currently support execution environments are:

* zsh
* sh
* python (requires the appropriate python interpreter installed at the system level)

If a `command` field is included in the definition this will represent a single line command to run for example `echo "hello world"`.

If a `script` field is included then this will be the path of a script relative to the tb.sdef file to execute.

The `argument` field will specify the argument that can be passed on the CLI to execute this particular command. The argument can be any length and combination of characters desired. These should be unique within the defintions file.

## Usage

```txt
Usage: toolbelt [OPTIONS] [ARG] -- [TOOL ARGS]

Options:
    --help      For help on toolbelt or a particular tool.
    --verbose   Enable verbose logging for toolbelt.

Arg: Is the argument of the corresponding tool in the toolbelt tb.sdef file. use --help with an arg to get help on that specific argument.
If no argument is specified the cli tool will run in interactive mode showing all available options and allowing one to be chosen.

Tool Args: any arguments after the optional -- delimiter will be passed directly through to the underlying tool.
```

### Examples

#### Run a tool from the CLI  

`toolbelt -b`  
where -b is the argument of a tool defined in the tb.sdef file you want to run.

#### Run toolbelt in interactive mode

`toolbelt`

#### Run a tool with optional arguments

`toolbelt -b -- -v some/path/to/file.txt`
where everything after the `--` delimiter will be passed through to the underlying tool.

#### Get help for a specific tool

`toolbelt --help -b`

#### Get toolbelt help and a list of available tools

`toolbelt --help`
