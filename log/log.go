package log

import (
	"log"

	"gitlab.com/tool.belt/toolbelt/environment"
)

var verbose = true

func Init(opts environment.Options) {
	verbose = opts.Verbose
}

func Verbosef(format string, args ...interface{}) {
	if verbose {
		Printf(format, args...)
	}
}

func Printf(format string, args ...interface{}) {
	log.Printf(format, args...)
}

func Fatalf(format string, args ...interface{}) {
	log.Fatalf(format, args...)
}
